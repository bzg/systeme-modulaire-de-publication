# Fiche synthétique : Simondon, Gilbert, Du mode d'existence des objets techniques

## Notice bibliographique de l'ouvrage étudié
Simondon Gilbert  
*Du mode d'existence des objets techniques*  
Paris, Aubier, 2012

## Plan de cette fiche
- [Notes](/notes.md)
- [Fiche synthétique](/fiche-synthetique.md)
- [Commentaire de texte](/commentaire-de-texte.md)

## Ressources
La fiche synthétique et le commentaire de texte s'appuie sur des émissions radiophoniques et des captations vidéo, en plus du livre de Gilbert Simondon :

- [Du mode d’existence d’un penseur technique, Gilbert Simondon (1/4), Les Nouveaux chemins de la connaissance, France Culture, 4 avril 2016](https://www.franceculture.fr/emissions/les-nouveaux-chemins-de-la-connaissance/gilbert-simondon-14-du-mode-d-existence-d-un)
- [Gilbert Simondon sur les objets techniques "ouverts" et "fermés" (1967), YouTube](https://www.youtube.com/watch?v=ScotWBb6ROo)
