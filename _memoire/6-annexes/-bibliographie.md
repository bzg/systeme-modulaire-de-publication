Cette bibliographie est également disponible sur Zotero sous forme d'un groupe, des références peuvent être ajoutées :

[https://www.zotero.org/groups/2191614/vers_un_systme_modulaire_de_publication/items](https://www.zotero.org/groups/2191614/vers_un_systme_modulaire_de_publication/items)

## Ouvrages

{% bibliography --query @book @incollection %}

## Articles

{% bibliography --query @article @inproceedings %}

<div class="break"></div>
## Thèses

{% bibliography --query @phdthesis %}

## Billets de blog, conférences, vidéos, émissions de radio, divers

{% bibliography --query @misc %}
